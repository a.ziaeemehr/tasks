import pandas as pd
from tasks.utility import to_influx
from influxdb import InfluxDBClient

symbol = "XBTUSD"
kline_size = "5m"
filename = '%s-%s-data.csv' % (symbol, kline_size)
# filename = "sample.csv"
to_influx("localhost", 8086, "mydb", filename, create_database=False)

