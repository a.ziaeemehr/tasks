# IMPORTS
import pandas as pd
import math
import os.path
import time
from bitmex import bitmex
from binance.client import Client
from datetime import timedelta, datetime
from dateutil import parser
from tqdm.notebook import tqdm
from tasks.utility import get_all_bitmex

# API
bitmex_api_key = 'l__wTyXjtFA2l5oUiEbtwpq6'
bitmex_api_secret = 'vRmotbfv7D4xMReI2JXjtJuaa2y8r492NTqueAceTNlxS0hw'

# CONSTANTS
binsizes = {"1m": 1, "5m": 5, "1h": 60, "1d": 1440}
batch_size = 750
bitmex_client = bitmex(test=False,
                       api_key=bitmex_api_key,
                       api_secret=bitmex_api_secret)

par = {
    'save': True,
    'symbol': "XBTUSD",
    'kline_size': "5m",
    'source': "bitmex",
    'binsizes': binsizes,
    'batch_size': batch_size,
    'bitmex_client': bitmex_client
}

data = get_all_bitmex(par)
