from cryptofeed import FeedHandler
from cryptofeed.exchanges import Binance
from cryptofeed.callback import BookCallback, TickerCallback, TradeCallback
from cryptofeed.defines import BID, ASK, FUNDING, L2_BOOK, OPEN_INTEREST, TICKER, TRADES
from cryptofeed.exchanges import Deribit


info = Binance.info()
info.keys()
# dict_keys(['symbols', 'channels', 'tick_size'])
info['chanels']
# ['l2_book', 'ticker', 'trades']
info['tick_size']['BTC-USDT']
# '0.01000000'


# demo_deribit.py
f = FeedHandler()
# Deribit can't handle 400+ simultaneous requests, so if all
# instruments are needed they should be fed in the different calls
sub = {TRADES: ["BTC-PERPETUAL"], TICKER: ['ETH-PERPETUAL'],
       FUNDING: ['ETH-PERPETUAL'], OPEN_INTEREST: ['ETH-PERPETUAL']}
f.add_feed(Deribit(subscription=sub,
                   callbacks={OPEN_INTEREST: oi,
                              FUNDING: funding,
                              TICKER: TickerCallback(ticker),
                              TRADES: TradeCallback(trade)}))
f.add_feed(Deribit(symbols=['BTC-PERPETUAL'],
                   channels=[L2_BOOK],
                   callbacks={L2_BOOK: BookCallback(book)}))


# parameter definitions
'''
max_depth: int
    Maximum number of levels per side to return in book updates
book_interval: int
    Number of updates between snapshots. Only applicable when book deltas are enabled.
    Book deltas are enabled by subscribing to the book delta callback.
snapshot_interval: bool/int
    Number of updates between snapshots. Only applicable when book delta is not enabled.
    Updates between snapshots are not delivered to the client
'''


# TickerInflux

# asynco, await
# https://realpython.com/async-io-python/
# courses:

https: // mit-online.getsmarter.com/presentations/lp/mit-cryptocurrency-online-short-course /?cid = 6460151415 & utm_contentid = 379123494893 & ef_id = c: 379123494893_d: c_n: g_ti: kwd-362438201433_p: _k: % 2Bcryptocurrency % 20 % 2Beducation_m: b_a: 76450813639 & gclid = CjwKCAjwuIWHBhBDEiwACXQYsR-25Zoq3SMvF2SuL1856FM4YVGT2zdCxqN-cPuB4g032IBTH7qvehoCxX0QAvD_BwE & gclsrc = aw.ds
https: // www.forbes.com/sites/rogerhuang/2019/08/12/eleven-free-courses-to-learn-bitcoin-blockchain-and-cryptocurrencies /?sh = a02f84250a72
https: // www.investopedia.com/best-cryptocurrency-trading-courses-5111984
https: // coinmetro.com/blog/best-sources-for-learning-about-crypto/
