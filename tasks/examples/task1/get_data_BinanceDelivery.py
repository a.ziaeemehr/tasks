'''
Copyright (C) 2018-2021  Bryant Moscon - bmoscon@gmail.com

Please see the LICENSE file for the terms and conditions
associated with this software.
'''
from cryptofeed import FeedHandler
from cryptofeed.backends.influxdb import BookDeltaInflux, BookInflux, FundingInflux, TickerInflux, TradeInflux
from cryptofeed.defines import BOOK_DELTA, FUNDING, L2_BOOK, TICKER, TRADES
from cryptofeed.exchanges import Bitmex, Coinbase
from cryptofeed.exchanges import BinanceDelivery


def main():

    info = BinanceDelivery.info()
    # print(info['symbols'])
    # ['BTC-USD_PERP', 'BTC-USD_210924', 'BTC-USD_211231', 'ETH-USD_PERP', 'ETH-USD_210924', 'ETH-USD_211231', 'LINK-USD_PERP', 'BNB-USD_PERP', 'TRX-USD_PERP', 'DOT-USD_PERP', 'ADA-USD_PERP', 'EOS-USD_PERP', 'LTC-USD_PERP', 'BCH-USD_PERP', 'XRP-USD_PERP', 'ETC-USD_PERP', 'FIL-USD_PERP', 'EGLD-USD_PERP', 'DOGE-USD_PERP', 'ADA-USD_210924', 'LINK-USD_210924', 'BCH-USD_210924', 'DOT-USD_210924', 'XRP-USD_210924', 'LTC-USD_210924', 'BNB-USD_210924', 'UNI-USD_PERP', 'THETA-USD_PERP', 'XLM-USD_PERP', 'ADA-USD_211231', 'LINK-USD_211231', 'BCH-USD_211231', 'DOT-USD_211231', 'XRP-USD_211231', 'LTC-USD_211231', 'BNB-USD_211231']


    f = FeedHandler()
    f.add_feed(BinanceDelivery(channels=[L2_BOOK, TRADES, TICKER],
                               symbols=['BTC-USD_PERP', 'THETA-USD_PERP'], #info['symbols'][-1]
                               callbacks={L2_BOOK: BookInflux('http://localhost:8086', 'example',
                                                              create_db=True,
                                                              numeric_type=float),
                                          TRADES: TradeInflux('http://localhost:8086', 'example',
                                                              create_db=True, numeric_type=float),
                                          TICKER: TickerInflux('http://localhost:8086', 'example',
                                                               create_db=True, numeric_type=float)}))

    f.run()


if __name__ == '__main__':
    main()
