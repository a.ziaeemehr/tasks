'''
Copyright (C) 2018-2021  Bryant Moscon - bmoscon@gmail.com

Please see the LICENSE file for the terms and conditions
associated with this software.
'''
from copy import deepcopy
from datetime import datetime
from cryptofeed import FeedHandler
from cryptofeed.defines import BOOK_DELTA, FUNDING, L2_BOOK, TICKER, TRADES
from cryptofeed.exchanges import Binance  # BinanceDelivery,
from cryptofeed.defines import BID, ASK, BOOK_DELTA, L2_BOOK, L3_BOOK
from cryptofeed.exchanges import Binance
from cryptofeed.backends.influxdb import (BookDeltaInflux,
                                          BookInflux, FundingInflux, TickerInflux, TradeInflux)
# from cryptofeed.callback import BookCallback, BookUpdateCallback


def main():

    # info = Binance.info()
    # print(info['symbols'])

    f = FeedHandler()

    f.add_feed(Binance(max_depth=5, book_interval=30,
        symbols=['BTC-USDT'], channels=[L2_BOOK, TICKER, TRADES],
        callbacks={L2_BOOK: BookInflux('http://localhost:8086', 'example1',
                                       create_db=True,
                                       numeric_type=float),
                   TRADES: TradeInflux('http://localhost:8086', 'example1',
                                       create_db=True, numeric_type=float),
                   TICKER: TickerInflux('http://localhost:8086', 'example1',
                                        create_db=True, numeric_type=float)
                   }))

    f.run()


if __name__ == '__main__':
    main()
