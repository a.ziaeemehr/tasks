from datetime import datetime
from cryptofeed import FeedHandler
from cryptofeed.defines import L2_BOOK, TICKER, TRADES
from cryptofeed.exchanges import BinanceDelivery

info = BinanceDelivery.info()
# info.keys : ['tick_size', 'contract_type', 'symbols', 'channels']

tfile = open("trades.txt", "w")
afile = open("abook.txt", "w")
tifile = open("ticker.txt", "w")

async def abook(feed, symbol, book, timestamp, receipt_timestamp):
    string = f'BOOK lag: {receipt_timestamp - timestamp} Timestamp: {datetime.fromtimestamp(timestamp)} Timestamp: {datetime.fromtimestamp(receipt_timestamp)} Feed: {feed} Pair: {symbol} Snapshot: {book}'
    print(string)
    afile.write(string)
    afile.write("\n")


async def ticker(feed, symbol, bid, ask, timestamp, receipt_timestamp):
    string = f'Feed: {feed} Pair: {symbol} Bid: {bid} Ask: {ask}'
    print(string)
    tifile.write(string)
    tifile.write("\n")

# async def ticker(**kwargs):
#     print(kwargs)


async def trades(feed, order_id, symbol, side, amount, price, timestamp, receipt_timestamp):
    string = f'Feed: {feed} Pair: {symbol} Side: {side} Amount: {amount} Price: {price} Timestamp: {datetime.fromtimestamp(timestamp)}'
    # print(string)
    tfile.write(string)
    tfile.write("\n")
    

def main():
    f = FeedHandler()
    f.add_feed(BinanceDelivery(max_depth=1, symbols=[info['symbols'][-1]],
                               channels=[L2_BOOK, TRADES, TICKER],
                               callbacks={L2_BOOK: abook, TRADES: trades, TICKER: ticker}))  # 
    f.run()


if __name__ == '__main__':
    main()
